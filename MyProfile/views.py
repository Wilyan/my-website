from django.shortcuts import render,redirect
from .forms import EventForm
from .models import EventModel

def homepage(request):
    return render(request, "Homepage.html", {})

def portofolio(request):
    return render(request, "Portofolio.html", {})

def guestbook(request):
    return render(request, "Guest-book.html", {})

def eventFormPost(request):
    if request.method ==  "POST":
        form = EventForm(request.POST)
        if(form.is_valid()):
            form_item = form.save(commit=False)
            form_item.save()
            form = EventForm()
            return redirect('/MyProfile/eventform/')
    else:
        form = EventForm()
    return render(request, "eventform.html", {'form': form})

def eventList(request):
    allEvent = EventModel.objects.all().order_by('date')
    return render(request, "eventlist.html", {'allEvent': allEvent})

def eventListDelete(request):
    allEvent = EventModel.objects.all().delete()
    return redirect('/MyProfile/eventlist/')
