from django.db import models
    
class EventModel(models.Model):
    date = models.DateTimeField()
    event = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    category = models.CharField(max_length=100)
        
    def __str__(self):
        return self.event
