from django import forms
from .models import EventModel

class EventForm(forms.ModelForm):
    date = forms.DateTimeField(widget=forms.DateTimeInput(
        attrs={
            'class':'form-control',
            'placeholder':'YYYY-MM-DD hh:mm',
            'type':'datetime-local'
            
        }
    ),input_formats=['%Y-%m-%dT%H:%M'])
    event = forms.CharField(widget=forms.TextInput(
        attrs={
            'class':'form-control',
            'placeholder':'What is the event...'
        }
    ))
    place = forms.CharField(widget=forms.TextInput(
        attrs={
            'class':'form-control',
            'placeholder':'Where it will be...'
        }
    ))
    category = forms.CharField(widget=forms.TextInput(
        attrs={
            'class':'form-control',
            'placeholder':'What is the catagory...'
        }
    ))

    class Meta:
        model = EventModel
        fields = ('date', 'event', 'place', 'category')